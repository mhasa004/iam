function [net,beta,perf]=svm_learn_pp(data,label,tdata,tlabel,T,enn,ebeta,eperf)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% learn_pp.m inputs:
%   data   - the training data matrix where each column corresponds to one
%            instance (features by instances)
%   class  - correct class information corresponding to the training data,
%            this is one dimentional matrix with correct labels.
%   tdata  - the testing data matrix in the same form as 'data'
%   tclass - the correct class information for the test data in the same
%            form as 'class'
%   eg     - error goal for MLP
%   hln    - number of hidden layer nodes
%   T      - number of classifiers to add to the ensemble
%   enn    - existing neural networks (incremental learning)
%   ebeta  - existing beta values (incremental learning)
%   eperf  - existing performance values (incremental learning)
%
%
% learn_pp will train 'T' classifiers on random subsets of 'data' and plot
% the performance on 'tdata' with the addition of each classifier.  The
% plot will show the overall performance (blue) as well as the performance
% on each class. When used in an incremental learning mode, this program
% needs to be run again with the outputs of the previous run. Note that at
% each incremental learning session, the outputs "net", "beta" and "perf"
% become the "enn", "ebata" and "eperf" for the next run, where new data
% become available. The inputs "data" and "class" should also change each
% run (presumably, becuase new training data is now available), but the
% "tdata" and "tclass" may remain the same, or be new.

% Note: for non-incremental learning, or the first session of an 
% incremental learning do not provide the function with the last
% three inputs.
%
%
% outputs:
%   net   - the neural network ensemble in a cell array
%   beta  - the weight values corresponding to each classifier 
%   perf  - the performance values after the addition of each classifier
%           where the first row is the overall performance and the following rows
%           represent the performance on individual classes
%
% New Implementation by Muhlbaier-Topalis  2004 based on original code by Robi Polikar 2001.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
T0=1; % Starting index for counting classifiers

N = size(data, 1);
dist = ones(1,N)/N;  %Initial distribution is uniform
A = size(tlabel,1);
cc = label;  %correct classes for the training data
cct = tlabel;   %Correct classes for the test data
if nargin<5
    sprintf('not enough input arguments');
elseif nargin==8   %If there is previously trained networks, i.e., this is incremental learning
    T0=length(ebeta)+1;
    net=enn;
    beta=ebeta;
    T=T+T0-1;
    perf=eperf;
    %Evaluate the exisiting ensemble on data
	ctr=svm_wmvote(net,beta,cc,data);  %Call weighted majority voting
	ERROR = sum(dist(ctr~=cc));
    BETA=ERROR/(1-ERROR);   %Normalize the ensemble error to [0 1] interval
    if ERROR==0
        ERROR=1e-6;  %Make sure that the error never hits zero, since 1/error =inf
    end
    dist(ctr==cc)=BETA*dist(ctr==cc);  %Adjust distribution for the new initialization
    dist = dist+1e-5;
	dist=dist/sum(dist);  %Renormalize distribution
end

%Start generating new classifiers

for i=T0:1:T
    error=1;
    while(error>=0.5)  %If the error > 1/2, select new data subset and create a new classifier
        index=seldata(dist,round(2*N/3));  %Choose 2/3 of the data to be the training data subset
        net{i}=weaksvm(data(index,:),label(index));
        c = svmpredict(cc, data, net{i}, '-q'); 
        error = sum(dist(c~=cc)); %Error of this classifier
    end
    beta(i)=error/(1-error);   %Normalize the individual error to [0 1] interval

    ctr=svm_wmvote(net,beta,cc,data);
    ERROR=sum(dist(ctr~=cc));
    if ERROR==0
        ERROR=1e-6;  %Make sure that the error never hits zero, since 1/error =inf
    end
    BETA=ERROR/(1-ERROR);  %Normalize the ensemble error to [0 1] interval
    dist(ctr==cc)=BETA*dist(ctr==cc);
    dist=dist/sum(dist);
    ct=svm_wmvote(net,beta,cct,tdata);
    perf(1,i)=length(find(ct==cct))/length(cct);
    for j=2:A+1
        perf(j,i)=length(find(ct(ct==cct)==(j-1)))/length(find(cct==(j-1)));
    end
    %plot(perf');
    %plot(perf(1,:)', 'LineWidth', 2);
    %grid on
    %axis([1 T 0 1])
    %title('Performance on Test Data')
    %xlabel('Number of classifiers')
    %ylabel('Classification Performance (%)')
    %getframe;
end



% Michael Muhlbaier and Apostolos Topalis
% November 2003
% All rights reserved, MAINC
%
% The following function randomly selects n data points according to a
% given distribution, only the indecies will be returned
% Original code and implementation by Robi Polikar , 2001.
function selected=seldata(pdist,n)

rand('state',sum(100*clock)) %randomize the random number generator

for j=1:n,          %find n examples
    index=rand;     % a random number to be used as an index            
    num=0;
    continU=1;
    h=1;
    while(continU==1)
        if (num<index&&index<(num+pdist(h)))    %test to see if index is between two examples
            selected(j)=h;                      %if index is between two examples add
            continU=0;                          %that first example to the selected examples
        end                                     
        num=num+pdist(h);
        h=h+1;
    end                             
    pdist(h-1)=0;               
    pdist=pdist./sum(pdist);  %renormalize so that pdist is a proper distribution
end




function classification=svm_wmvote(net,beta,label,data)
% wmvote takes a cell array of neural networks ('net') and their corresponding weight
% values ('beta') and uses weighted majority voting to classify 'data' 

[N,M] = size(data);
T=length(beta);
outcome=zeros(M,N,T);
for i=1:T
    cte = svmpredict(label, data, net{i}, '-q');
    %fprintf(repmat('\b', 1, 50));
    for j=1:N
        outcome(cte(j),j,i)=log(1/(beta(i)+.0001));
    end
end
[~, classification]=max(sum(outcome,3));
classification = classification';

function net=weaksvm(data, class)

%--search of the best parameters
net = svmtrain(class, data, '-t 0 -q');
