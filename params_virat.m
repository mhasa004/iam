clear all; close all;

addpath('c:/tools/libsvm-3.20/windows');

load('data/allEvents_virat.mat');
pc.dataset = 'virat';
pc.numSeq = length(allEvents);
pc.numClasses = 12;
pc.T = 5;

pc.strongTeacher = 1;
pc.weakTeacher = 1;
pc.delta = 0.9;
pc.K = 0.4;
pc.fixedBuffer = true;

pc.numBatch = 5;
pc.trainSeq = [1:7,301,190,252,253,63:84,258,259,8:36,85:114,37:62,115:170];
pc.testSeq = setdiff(1:pc.numSeq, pc.trainSeq);

run incremental_learning.m

% run learningWithContextAOPOvALV6.m
% for i = 2:2:20
%     pc.T = i;
%     pc.dataset = sprintf('viratT%0.2d',i);
%     run incremental_learning.m
% end
% 
% pc.strongTeacher = 1;
% pc.weakTeacher = 1;
% pc.T = 5;
% for i = 1:10
%     pc.K = i/10;
%     pc.dataset = sprintf('viratK%0.2d',i);
%     run incremental_learning.m
% end

% pc.strongTeacher = 1;
% pc.weakTeacher = 1;
% for d = 0.5:0.1:1.0
%     pc.delta = d;
%     pc.dataset = sprintf('viratD%0.2f',d);
%     run incremental_learning.m
% end