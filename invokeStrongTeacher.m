function strongIndex = invokeStrongTeacher(h)
    if size(h,1) == 0
        strongIndex = [];
    elseif size(h,1) == 1
        strongIndex = 1;
    else
        h1 = sort(h);
        diff = h1(end,:)-h1(end-1,:);
        strongIndex = diff<0.2;
    end
end