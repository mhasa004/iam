fprintf('%s dataset\n',pc.dataset);
fprintf('Strong Teacher - %d, Weak Teacher - %d\n', pc.strongTeacher, pc.weakTeacher);

% Arrange test data
test_features = [];
test_labels = [];

for i = pc.testSeq
    for j = 1:allEvents{i}.numEvents
        if abs(sum(allEvents{i}.features(:,j))) > 0
            test_features = [test_features, allEvents{i}.features(:,j)];
            test_labels = [test_labels, allEvents{i}.eventTypes(j)];
        end
    end
end

% Compute number of training instances
num_train_instances = 0;
for i = pc.trainSeq
    for j = 1:allEvents{i}.numEvents
        if abs(sum(allEvents{i}.features(:,j))) > 0
           num_train_instances = num_train_instances + 1;
        end
    end 
end
train_ins_per_iter = ceil(num_train_instances / pc.numBatch);
fprintf('# of instances per batch: %d\n', train_ins_per_iter)

% Learn incrementally
% Initialization of edge cotnext matrices
buffer_train_features = [];
buffer_train_labels = [];
acc1 = []; acc2 = []; acc_nc = [];

res = [];
test_probs = [];

total_train_ins = 0;
total_man_labeled_ins = 0;
iteration = 1;
cur_train_seq = [];

for i = pc.trainSeq    
    % Arrange data for the new video sequence 
    new_train_features = [];
    new_train_labels = [];
    for j = 1:allEvents{i}.numEvents
        if abs(sum(allEvents{i}.features(:,j))) > 0
            % Store new examples in buffer
            new_train_features = [new_train_features, allEvents{i}.features(:,j)];
            new_train_labels = [new_train_labels, allEvents{i}.eventTypes(j)];
            total_train_ins = total_train_ins + 1;
        end
    end
    
    if iteration > 1
        if ~isempty(new_train_features)
            % Get predictions from the current appearance model
            
            [pred, h] = svm_wmvote_outcome(net, beta, new_train_labels', new_train_features', pc.numClasses);
            weak_labels = pred;
            
            % Active Learning Module
            h = h./repmat(sum(h),size(h,1),1);
            [weak_index, strong_index] = al_module(h, pc.K, ...
                pc.delta, pc.strongTeacher, pc.weakTeacher);
            
            % Store the newly labeled data in the buffer
            buffer_train_features = [buffer_train_features, new_train_features(:,strong_index)];
            buffer_train_labels = [buffer_train_labels, new_train_labels(strong_index)];
            buffer_train_features = [buffer_train_features, new_train_features(:,weak_index)];
            buffer_train_labels = [buffer_train_labels, weak_labels(weak_index)];
            
            total_man_labeled_ins = total_man_labeled_ins + length(strong_index);
        end
    else       
        % Store new examples in buffer for initial training
        buffer_train_features = [buffer_train_features, new_train_features];
        buffer_train_labels = [buffer_train_labels, new_train_labels];        
        total_man_labeled_ins = total_man_labeled_ins + length(new_train_labels);
    end
    
    cur_train_seq = [cur_train_seq, i];
    % If buffer is full update the baseline classifier
    if total_train_ins >= iteration*train_ins_per_iter || i==pc.trainSeq(end) 
        % Update the appearance model
        fprintf('# Training instances: %d, ', length(buffer_train_labels));
           
        if iteration == 1
            [net, beta, perf] = svm_learn_pp(buffer_train_features', buffer_train_labels', test_features', test_labels', pc.T);
        else
            [net,beta,perf] = svm_learn_pp(buffer_train_features', buffer_train_labels', test_features', test_labels', pc.T, net, beta, perf);
        end
        % Get the labels from the appearance model 
        [pred, h] = svm_wmvote_outcome(net, beta, test_labels', test_features', pc.numClasses);
        
        iteration = iteration + 1;

        % [~,res_a] = max(h,[],1);
        acc_nc = [acc_nc, mean(test_labels == pred)];
        

        fprintf('Accuracy: %0.3f\n', acc_nc(end)*100);
         
        res = [res; pred];
        test_probs = cat(3,test_probs,h);
        
        if pc.fixedBuffer 
            buffer_train_features = [];
            buffer_train_labels = [];
        end
        
    end
end
fprintf('Number of manually labeled instances: %d/%d\n', total_man_labeled_ins,total_train_ins);
fprintf('Number of test instances: %d\n', length(test_labels));
