function strongIndex = invokeStrongTeacher_cvpr(h, K)
    h1 = sort(h, 2, 'descend');
    h1 = h1(:, 1:2);
    cc = h1(:,1) - h1(:,2);
    [~,idx] = sort(cc);
    strongIndex = idx(1:round(length(idx)*K));
end