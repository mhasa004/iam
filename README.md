# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

This repository contains the code to reproduce the result published in the following paper.

Mahmudul Hasan and Amit K. Roy-Chowdhury, Incremental Activity Modeling and Recognition in Streaming Videos, CVPR 2014, Columbus, Ohio, USA.

The current version of the code will produce better result from the above mentioned paper because of the use of improved feature.

### How do I get set up? ###

The code is entirely written in Matlab.

params_virat.m is the starting point of the program. We have provided all the processed features for VIRAT dataset in data folder. Different parameters can be changed in params_virat.m folder and then, call the incremental_learning.m file for running the incremental learning algorithm.