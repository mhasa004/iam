function [weakIndex, strongIndex] = al_module(h, K, ...
    delta, strongTeacher, weakTeacher)

numANodes = size(h,2);

% Weak teacher
weakIndex = [];
if weakTeacher == 1
    weakIndex = find(max(h)>delta);
end

% Strong teacher
strongIndex = [];
if strongTeacher == 1
    h = h';
    strongIndex = invokeStrongTeacher_cvpr(h, K);
end

% All manual labeling
if weakTeacher == 0 && strongTeacher == 0
    strongIndex = 1:numANodes;
end

% Only entropy
if weakTeacher == 3 && strongTeacher == 3
    strongIndex = invokeStrongTeacher_e(h, K);
end

strongIndex = setdiff(strongIndex,weakIndex);