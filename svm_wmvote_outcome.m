function [pred, probs] = svm_wmvote_outcome(net,beta,label,data,num_class)
% wmvote takes a cell array of neural networks ('net') and their corresponding weight
% values ('beta') and uses weighted majority voting to classify 'data' 

[N,M] = size(data);
T=length(beta);
probs=zeros(M,N,T);
for i=1:T
    cte = svmpredict(label, data, net{i}, '-q');
    for j=1:N
        probs(cte(j),j,i)=log(1/(beta(i)+.0001));
    end
end
[~, pred]=max(sum(probs,3));
probs = sum(probs,3);
probs = probs(1:num_class,:);
