function strongIndex = invokeStrongTeacher_e(h, K)
    h1 = entropy(h);
    [~, strongIndex] = sort(h1);
    count = length(strongIndex);
    strongIndex = strongIndex(1:round(count*K));
end